import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { CHARACTERS } from './rec-tstdata';

import { HttpClient } from '@angular/common/http';

@Injectable()

export class CapcocsvdataService {
constructor(private httpClient: HttpClient) { }
getCharacters(): Observable<any[]>{
  return Observable.of(CHARACTERS).delay(100);
}

getColumns(): string[]{
  return [
  "name","phone","email","company","date_entry","org_num",
  "address_1","city","zip","geo","pan","pin","id","status","fee",
  "guid","date_exit","date_first","date_recent","url"
  ]};

sendPostRequest(data: any): Observable<any> {
     return this.httpClient.post<any>('/api/submit', data);
}

}
