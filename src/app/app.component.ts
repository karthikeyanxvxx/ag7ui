import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import {HttpClient} from '@angular/common/http';

@Component({  
  selector: 'app-root',  
  templateUrl: './app.component.html',  
  styleUrls: ['./app.component.css']  
})  
  
export class AppComponent implements OnInit {  
  title = 'Customer List';
  selectedRow : Number;
  setClickedRow : Function;
  p: number = 1;
  public arrJRecords: string [];
  public selectedRecord: any;

  selectedRow1: any;
  selectedAll: boolean = false;

  RecId : number;
  RecSts : string;

  constructor (private httpService: HttpClient) {
  this.selectedRow1 = [];
  }

  ngOnInit () {

    this.httpService.get('./assets/sample_data.json').subscribe(
      data => {
        this.arrJRecords = data as string [];	 
        //console.log(this.arrJRecords[1]);
      },
      (err: HttpErrorResponse) => {
        console.log (err.message);
      }
    );

    this.setClickedRow = function(index){
            this.selectedRow = index;
    }  

  }

  RowSelected(u:any){
    this.selectedRecord=u;
  }

  selectAll(index) {
    if (typeof (index) == 'undefined') {
    this.selectedAll = !this.selectedAll;
    this.selectedRow1 = [];
    } else {
    this.selectedRow1.push(index);
    }
  }

  sendPost(rec:any) {
    console.log(rec.id);
    this.httpService.post<any>('/api/submit', { title: 'Capco UI Test' }).subscribe(data => { this.RecId = rec.id; this.RecSts = rec.status; });

//    this.capcosrv.sendPostRequest(rec).subscribe(rec => 
//    { console.log(rec));

  }

}
