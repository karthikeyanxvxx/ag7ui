import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { AppComponent } from './app.component';

import { CapcocsvdataService } from './capcocsvdata.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, NgxPaginationModule,
    HttpClientModule,
    RouterModule.forRoot([])
  ],
  providers: [CapcocsvdataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
