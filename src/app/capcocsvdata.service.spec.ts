import { TestBed } from '@angular/core/testing';

import { CapcocsvdataService } from './capcocsvdata.service';

describe('CapcocsvdataService', () => {
  let service: CapcocsvdataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CapcocsvdataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
